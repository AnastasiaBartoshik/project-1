import math


# task_1_1
"""
Даны 2 действительных числа a и b. Получить их сумму,
разность и произведение.
"""

a = 2
b = 3
c = a + b
print("сумма", c)
d = a - b
print("разность", d)
e = a * b
print("произведение", e)


# task_1_2
"""
Даны действительные числа x и y.
Получить |x| − |y| / 1+ |xy|
"""

x = 6
y = 5
a = (abs(x) - abs(y)) / (1 + abs(x * y))
print("ответ", a)


# task_1_3
"""
Дана длина ребра куба. Найти объем куба и
площадь его боковой поверхности.
"""

a = 6
v = a ** 3
print("обьем куба", v)
s = 4 * a ** 2
print("площадь боковой поверхности", s)


# task_1_4
"""
Даны два действительных числа. Найти среднее арифметическое и
среднее геометрическое этих чисел.
"""

a = 5
b = 5
c = (a + b) / 2
print("среднее арифметическое", c)
d = math.sqrt(a * b)
print("среднее геометрическое", d)


# task_1_5
"""
Даны катеты прямоугольного треугольника.
Найти его гипотенузу и площадь.
"""

a = 3
b = 4
c = math.sqrt(a ** 2 + b ** 2)
print("гипотенуза", c)
s = 1 / 2 * (a * b)
print("площадь", s)
